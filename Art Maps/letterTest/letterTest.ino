#include <FastLED.h>
//virker med ESP8266
#define DATA_PIN     5
#define COLOR_ORDER GRB
#define CHIPSET     WS2811
#define NUM_LEDS    300

CRGB leds[NUM_LEDS];

void setup()
{
  FastLED.addLeds<NEOPIXEL,DATA_PIN>(leds, NUM_LEDS);
}

void loop() 
{
	// w
	leds[75] = CRGB(0, 0, 100);  
	leds[90] = CRGB(0, 0, 100);  
	leds[105] = CRGB(0, 0, 100);  

	leds[77] = CRGB(0, 0, 100);  
	leds[91] = CRGB(0, 0, 100);  

	leds[92] = CRGB(0, 0, 100);  
	leds[107] = CRGB(0, 0, 100);  

	leds[93] = CRGB(0, 0, 100);  
	leds[79] = CRGB(0, 0, 100);  

	// E
	leds[134] = CRGB(100, 0, 0);  
	leds[135] = CRGB(100, 0, 0);  
	leds[136] = CRGB(100, 0, 0);  

	leds[149] = CRGB(100, 0, 0);  
	leds[150] = CRGB(100, 0, 0);  

	leds[164] = CRGB(100, 0, 0);  
	leds[165] = CRGB(100, 0, 0);  
	leds[166] = CRGB(100, 0, 0);  
  FastLED.show();
  delay(1);
}
