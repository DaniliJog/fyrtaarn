#include <FastLED.h>
//virker med ESP8266
#define DATA_PIN     5
#define COLOR_ORDER GRB
#define CHIPSET     WS2811
#define NUM_LEDS    150

#define BRIGHTNESS  200
#define FRAMES_PER_SECOND 60

byte hue = 0;         // This variable will control the hue of the LEDs
int rnd = 0;          // A variable used to hold a random number
CRGB leds[NUM_LEDS];  // Create an Array of LEDs

void setup() { 
  FastLED.addLeds<NEOPIXEL,DATA_PIN>(leds, NUM_LEDS);  //Initialise the LED ring(s)
}

void loop() { 
  for( int i = 0; i < NUM_LEDS; i++) {
    
    if(i% 2 == 0){   
    leds[i] = CHSV(rand() % 35 + 15,255,120);
    }
    FastLED.show(); 
    if(i% 2 == 1){ 
    leds[i] = CHSV(rand() % 35 + 165,255,120);
    }
    
    FastLED.show(); 
    delay(1);

    }   
    }
