#include <FastLED.h>
//virker med ESP8266
#define DATA_PIN     5
#define COLOR_ORDER GRB
#define CHIPSET     WS2811
#define NUM_LEDS    150

byte hue = 0;
byte hue_negative = 255;
CRGB leds[NUM_LEDS];
int state = 0;

void setup()
{
  FastLED.addLeds<NEOPIXEL,DATA_PIN>(leds, NUM_LEDS);
}

void loop() 
{
  for(int i = 0; i < NUM_LEDS; i++) 
  {
    if(i% 2+state == 0){   
          leds[i] = CHSV(25,255,80);
      }
      FastLED.show(); 
      if(i% 2+state == 1){ 
          leds[150-i] = CHSV(180,255,80);
      }
  
  FastLED.show();
  delay(1);
  }
  switch(state) {
   case  0  :
      state = 1;
      break; //optional
   case  1  :
      state = 0;
      break; //optional
  }
  }
